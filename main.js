const eleContainer = document.getElementById('container')
const eleInput = document.getElementById('cityName')
const eleLoading = document.getElementById('loading')
const eleTitle = document.getElementById('title')
const eleConBot = document.querySelector('.container-bot')
const eleImagesWrap = document.querySelector('.other-img-wrap')
const btnOperation = document.querySelector('.btn-ops')

const btnRefresh = document.querySelector('#refresh')
const btnSubmit = document.getElementById('submit')
const btnScrollLeft = document.querySelector('#scroll-left')
const btnScrollRight = document.querySelector('#scroll-right')

const api_pre = 'https://api.unsplash.com/search/photos?query='
const api_sub = '&client_id='
const accessKey = '-5xjwbxb2JzQOmLYwgMnGKua3hOk_Wp2thETHJTlO7o'

const keyName = 'cities'
let color = null
let inputCity = null
let api = null
let rd = null
let imgData = null
let cityStorages = []
let typeCounter = 0
let page = 1


var fixedOptions = places({
    container: eleInput,
    templates: {
        value: function (suggestion) {
            return suggestion.name
        },
        suggestion: function (suggestion) {
            return suggestion.name
        }
    },
    style: false
})


//type the name of the city
function typeWriter(city) {
    if (typeCounter < city.length) {
        eleTitle.innerText += city.charAt(typeCounter);
        typeCounter++
        setTimeout(() => typeWriter(city), 200);
    } else {
        typeCounter = 0
    }
}

function hexToRgb(hex) {
    let bigint = parseInt(hex, 16);
    let r = (bigint >> 16) & 255;
    let g = (bigint >> 8) & 255;
    let b = bigint & 255;

    return [255 - r, 255 - g, 255 - b];
}

function getRandom(range) {
    return Math.floor(Math.random() * range)
}

function clearImgBorder() {
    let eleImgs = document.querySelectorAll('.selected')
    for (let i = 0; i < eleImgs.length; i++) {
        eleImgs[i].classList.remove('selected')
    }
}



const changeBg = (imgData, color, city) => {
    eleTitle.style.color = `rgb(${hexToRgb(color)[0]},${hexToRgb(color)[1]}, ${hexToRgb(color)[2]})`
    document.body.style.backgroundColor = `rgb(${hexToRgb(color)[0]},${hexToRgb(color)[1]}, ${hexToRgb(color)[2]})`

    setTimeout(() => {
        eleContainer.style.backgroundImage = `url('${imgData}')`;
        setTimeout(() => {
            eleContainer.style.opacity = '1'
            //launch the type writer
            typeWriter(city)
        }, 500)
    }, 800)

    eleInput.value = ''
    eleLoading.style.display = 'none'
}

const errorBg = () => {
    //error handing
    eleInput.value = 'Error...'
    imgData = 'url("https://images.unsplash.com/photo-1623018035782-b269248df916?crop=entropy&cs=srgb&fm=jpg&ixid=MnwyNDQ1NjN8MHwxfHNlYXJjaHw2fHxFUlJPUnxlbnwwfHx8fDE2MjU2MjUyODc&ixlib=rb-1.2.1&q=85")'

    setTimeout(() => {
        eleContainer.style.backgroundImage = imgData;
        eleInput.value = ''
        eleLoading.style.display = 'none'

        setTimeout(() => {
            eleContainer.style.opacity = '1'
        }, 500)
    }, 1000)
}

const displayAllImg = (datas, isNew) => {
    if (isNew) {
        eleImagesWrap.innerHTML = ''
    }
    datas.map(data => {
        let imgCard = document.createElement('div')
        imgCard.classList.add('animate__backInRight')
        imgCard.classList.add('animate__animated')
        imgCard.classList.add('imgCard')
        let img = document.createElement('img')
        if (datas.indexOf(data) === 0 && page === 1) {
            img.classList.add('selected')
        }
        img.src = data.urls.thumb
        imgCard.appendChild(img)
        imgCard.addEventListener('click', (evt) => {
            eleLoading.style.display = 'block'
            eleContainer.style.opacity = '0'
            eleTitle.innerText = ''
            clearImgBorder()
            evt.target.classList.add('selected')

            changeBg(data.urls.regular, data.color.replace('#', ''), inputCity)
            changeStorage(data.urls.regular, data.color.replace('#', ''), inputCity)
        })
        eleImagesWrap.append(imgCard)
    })
    if (!isNew) {
        setTimeout(() => eleConBot.scrollLeft += 10000000, 1000)
    }
}

function loadMemory() {
    let cities = JSON.parse(window.localStorage.getItem(keyName))
    if (!cities) {
        window.localStorage.setItem(keyName, JSON.stringify([{
                city: 'guangzhou',
                url: api_pre + 'guangzhou' + api_sub + accessKey,
                color: 'black'
            }])
        )
        changeBg('https://www.arch2o.com/wp-content/uploads/2015/08/Arch2O-case-study-the-parametric-twist-of-canton-tower-2.png', 'black', 'GUANGZHOU')
        return
    }
    if (cities.length === 1) {
        changeBg('https://www.arch2o.com/wp-content/uploads/2015/08/Arch2O-case-study-the-parametric-twist-of-canton-tower-2.png', 'black', 'GUANGZHOU')
        return
    }
    if (cities.length > 1) {
        let lastData = cities[cities.length - 1]
        let lastDataUrl = lastData.url
        let lastDataCity = lastData.city
        let lastDataColor = lastData.color
        changeBg(lastDataUrl, lastDataColor, lastDataCity)
    }

}

function changeStorage(imgData, color, city) {
    cityStorages = JSON.parse(window.localStorage.getItem(keyName))
    cityStorages.push({
        url: imgData,
        city: city,
        color: color,
    })
    window.localStorage.setItem(keyName, JSON.stringify(cityStorages))
}

const refreshBg = (city, page, isNew) => {
    //get api from input
    api = api_pre + city + api_sub + accessKey + '&page=' + page

    //fetch api
    fetch(api)
        .then(response => response.json())
        .then(data => {
            if (page === 1) {
                rd = getRandom(data.results.length)

                imgData = data.results[rd].urls.regular
                color = data.results[rd].color.replace('#', '')

                changeBg(imgData, color, inputCity)

                changeStorage(imgData, color, city)
            }
            btnOperation.style.display = 'flex'
            displayAllImg(data.results, isNew)

        })
        .catch((err) => errorBg())
}

//bind enter key to button click event
eleInput.addEventListener("keyup", event => {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        event.preventDefault();
        btnSubmit.click();
    }
});

//button click event
btnSubmit.addEventListener('click', () => {
    //reset the typewriter and loader to initial state
    page = 1
    eleLoading.style.display = 'block'
    eleContainer.style.opacity = '0'
    eleTitle.innerText = ''

    //change to upper case and launch the typewriter
    inputCity = eleInput.value.toUpperCase();

    refreshBg(inputCity, page, true)
})

btnRefresh.addEventListener('click', () => {
    page += 1
    refreshBg(inputCity, page, false)
})

btnScrollLeft.addEventListener('click', () => {eleConBot.scrollLeft -= 500})
btnScrollRight.addEventListener('click', () => {eleConBot.scrollLeft += 500})

loadMemory()
